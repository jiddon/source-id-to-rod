#!/usr/bin/python3
import argparse

"""
Use this to trace a source ID from a ROS back to its ROD / BOC.
Use daqslice Run Resources tab to find the source ID for a ROL.
Or find it in the ROS fragment dump.
"""

SEG_LUT = {'0x11':'Barrel', '0x12':'Disk', '0x13':'BLayer', '0x14':'IBL', '0x15':'DBM'}
CRT_LUT = {0:'B1', 1:'B2', 2:'B3', 3:'D1', 4:'L1', 5:'L2', 6:'D2', 7:'L3', 8:'L4'} # PIX only

def get_pix_rod(sid):
    seg = hex(int('0x'+sid[2:4], 16))
    lyr = hex(int('0x'+sid[4], 16))
    crt = hex(int('0x'+sid[5], 16))
    slt = int(sid[6:])    
    assert int(crt, 16) in CRT_LUT.keys(), "Invalid crate in source ID!"
    crate = CRT_LUT[int(crt,16)]
    segment = SEG_LUT[seg]
    lattitude = None
    if slt < 40:
        lattitude = 'north'
        slot = slt
    else:
        lattitude = 'south'
        slot = slt - 40
    assert lattitude, "Invalid slot, could not work out if north or south!"
    rod = crate+"_S"+str(slot)+"_"+lattitude
    return rod

def get_ibl_rod(sid):
    slot = int(sid[5:7])
    assert sid[-1] in ['0','1','2','3'], "Invalid slot, could not work out if north or south!"
    lattitude=None    
    if sid[-1] in ['2','3']:
        lattitude = 'south'
    elif sid[-1] in ['0', '1']:
        lattitude = 'north'
    rod = 'I1_S' + str(slot) + '_'+ lattitude
    return rod
    
def source_id_to_rod(sid):
    """ Convert source ID to ROD. """
    if len(sid) == 8:
        assert sid[:2] == '00', "Source ID not valid!"
    else:
        assert len(sid) == 6, "Source ID not valid!"
        sid = '00' + sid
    seg = hex(int('0x'+sid[2:4], 16))
    assert seg in SEG_LUT.keys(), "Invalid segment in source ID!"
    segment = SEG_LUT[seg]
    if segment == 'IBL':
        rod = get_ibl_rod(sid)
    else:
        rod = get_pix_rod(sid)
    print(rod)

    
if __name__=="__main__":
    parser = argparse.ArgumentParser(prog = 'ros_to_rod',
                                     description = 'Convert from source ID to ROD.')
    parser.add_argument('-s', '--source_id', required=True, help='Source ID found in the ROS fragment dump or in segments and resources. Must be six or eight digits where the latter has two leading zeros, e.g. 111707 or 00111707.')
    args = parser.parse_args()

    source_id_to_rod(args.source_id)
