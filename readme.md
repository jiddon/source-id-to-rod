# Overview
Use this to trace a source ID from a ROS back to its ROD / BOC.

# Usage
`python3 source_id_to_rod.py -s <source id>`

- Source ID must be six or eight digits where the latter has two leading zeros, e.g. 111707 or 00111707
- Use daqslice Run Resources tab to find the source ID for a ROL.
- Or find it in the ROS fragment dump.
